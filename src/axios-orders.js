import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger-234f3.firebaseio.com/'
});

export default instance;