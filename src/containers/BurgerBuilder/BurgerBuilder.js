import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from '../../axios-orders';

import Aox from '../../hoc/Aox/aox';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actionType from '../../store/actions/index';




class BurgerBuilder extends Component {

    state = {
        purchasing: false,
    }
    componentDidMount () {
      this.props.onInitIngredient();
    }

    updatePurchaseState ( ingredients ) {
        const sum = Object.keys( ingredients )
            .map( igKey => {
                return ingredients[igKey];
            } )
            .reduce( ( sum, el ) => {
                return sum + el;
            }, 0 );
        return  sum > 0;
    }

    purchaseHandler = () => {
      if(this.props.isLogin){
        this.setState( { purchasing: true } );
      }
      else{
        this.props.onSetAuthRedirectPath('/checkout');
        this.props.history.push('/auth');
      }
    }

    purchaseCancelHandler = () => {
        this.props.onInitPurchase();
        this.setState( { purchasing: false } );
    }

    purchaseContinueHandler = () => {
        this.props.history.push('/checkout');
    }

    render () {
        const disabledInfo = {
            ...this.props.ings
        };
        for ( let key in disabledInfo ) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }
        let orderSummary = null;
        // console.log(this.props.error);
        let burger = this.props.error ? <p>Ingredients can`t be loaded!</p> : <Spinner />;

        if ( this.props.ings ) {
            burger = (
                <Aox>
                    <Burger ingredients={this.props.ings} />
                    <BuildControls
                        ingredientAdded={this.props.onIngredientAdded}
                        ingredientRemoved={this.props.onIngredientRemoved}
                        disabled={disabledInfo}
                        purchasable={this.updatePurchaseState(this.props.ings)}
                        ordered={this.purchaseHandler}
                        isLogin={this.props.isLogin}
                        price={this.props.price} />
                </Aox>
            );
            orderSummary = <OrderSummary
                ingredients={this.props.ings}
                price={this.props.price}
                purchaseCancelled={this.purchaseCancelHandler}
                purchaseContinued={this.purchaseContinueHandler} />;
        }
        // {salad: true, meat: false, ...}
        return (
            <Aox>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aox>
        );
    }
}

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    error: state.burgerBuilder.error,
    isLogin: state.auth.token !== null
  };
}

const mapDispatchToProps = dispatch => {
  return{
    onIngredientAdded: (ingName) => dispatch(actionType.addIngredient(ingName)),
    onIngredientRemoved: (ingName) => dispatch(actionType.removeIngredient(ingName)),
    onInitIngredient: () => dispatch(actionType.initIngredients()),
    onInitPurchase: () => dispatch(actionType.purchaseInit()),
    onSetAuthRedirectPath: (path) => dispatch(actionType.setAuthRedirectPath(path))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));